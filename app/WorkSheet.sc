val x = ("a", 1)
val y = ("b", 2)

val m = Map( ("a", 1), ("b", 2) )
val l = List(1,2)
val a = Array(1,2)

m.getOrElse("b", 3)

val result = m.get("b")
if (result.nonEmpty) {
  result.get
} else {
  3
}
