package controllers

import java.sql.SQLTimeoutException

import com.google.inject.Inject
import db.schema.AcademicCalendar
import net.shantitree.flow.db.driver.MyPostgresDriver
import net.shantitree.flow.db.driver.MyPostgresDriver.api._
import play.api.db.slick.DatabaseConfigProvider
import play.api.mvc.{Action, AnyContent, Controller, Request}
import db.schema.Schemas.pub._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent.Future
import scala.util.{Failure, Success}
import upickle.default._
import pickle.CustomPickle._
import views.html.main

class Application @Inject() (dbConfigProvider: DatabaseConfigProvider) extends Controller {

  val dbConfig = dbConfigProvider.get[MyPostgresDriver]

  import dbConfig.driver.api._

  val db = dbConfig.db // ตัวแทนของ Database

  def index = Action { request ⇒
    Ok(main())
    /*
    val isLogin = request.session.data.get("login")
    if (isLogin.nonEmpty) {
      Ok("Hello RmutpBus App")
    } else {
      Forbidden("Please Login First!")
    }
    */
  }

  def isLogin(request: Request[AnyContent]): Boolean = {
    request.session.data.get("login").nonEmpty
  }

  def readUserId(request: Request[AnyContent]): String = {
    request.session.data.get("userName").get
  }



  def login(credentials: String) = Action.async {

    val xs = credentials.split("/")
    val userName = xs(0)
    val password = xs(1)

    val query = tqPersons
      .map(_.studentCode)
      .filter(_ === userName)

    val fResult = db.run(query.result)

    fResult.map { result ⇒
      if (result.nonEmpty) {
        Ok("pass").withSession("login" → "success", "userName" → userName)
      } else {
        Forbidden("Invalid UserName")
      }
    }
  }

  def academicCalendar = Action.async {
    val fResult = db.run(tqAcademicCalendar.result)
    fResult.map { result =>
      Ok(write(result))
    }

  }

  def lecturerCourseSchedule(academicYear: Int, semester: Int, lecturerName: String) = Action.async {
    val query = vqLecturerCourseSchedule.filter { t ⇒
      t.lecturerFirstName === lecturerName &&
      t.academicYear === academicYear &&
      t.semester === semester
    }

    db.run(query.result)
      .map( result ⇒ Ok(write(result)))
  }

  /* def lecturerCourseSchedule2(academicYear: Int, semester: Int, lecturerName: String) = Action.async {   //ไม่ต้องใช้รหัสในการล็อกอิน//
    val query = for {
      ((t1,t2),t3) <- tqCourses
        .join(tqSubjects).on(_.subjectId === _.id)              //_.จะเป็นตารางที่อยู่ทางขวาเสมอ//
        .join(tqPersons).on(_._1.lecturerId === _.id)
      if t3.firstName === lecturerName && t1.academicYear === academicYear && t1.semester === semester
    } yield {
      (t1.lectureRoom, t1.startLecturedAt, t2.code, t2.name, t3.lecturerPosition, t3.firstName, t3.lastName)
    }

    val fResult = db.run(query.result)
    fResult.map { result =>
      Ok(write(result))
    }
  }
  */

  def personalCourseSchedule(academicYear: Int, semester: Int) = Action.async { request =>
    if (isLogin(request)) {
      val userId = readUserId(request)
      val query = for {

      }
    }
  }




  def personalCourseSchedule2(academicYear: Int, semester: Int) = Action.async { request ⇒     //ใช้รหัสในการล็อกอินเพื่อขอเข้าดู//
    if (isLogin(request)) {
      val userId = readUserId(request)
      val query = for {
        ((((t1, t2), t3), t4), t5) ← tqCourseRegistries
          .join(tqCourses).on(_.courseId === _.id)
          .join(tqSubjects).on(_._2.subjectId === _.id)
          .join(tqPersons).on(_._1._2.lecturerId === _.id)
          .join(tqPersons).on(_._1._1._1.studentId === _.id)
        if t5.studentCode === userId && t2.academicYear === academicYear && t2.semester === semester
      } yield {
          (t2.startLecturedAt, t3.name, t3.code, t4.firstName, t4.lastName, t2.academicYear, t2.semester)
      }

      val fResult = db.run(query.result)
      fResult.map { result ⇒
        Ok(write(result))
      }
    } else {
      Future.successful(Forbidden("Hey!"))
    }
  }
}