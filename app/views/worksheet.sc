class Cat(color: String)

val cat = new Cat("red")

val xs = List(1,2,3,4)
xs.filter((a) => a > 2)
xs.map((a) => a+1)

val x1 = (a:Int, b:Int) => a+b

def x2(a:Int, b:Int) = a+b

x1(1,2)
x2(1,2)