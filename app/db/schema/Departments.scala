package db.schema

import slick.lifted.Tag
import net.shantitree.flow.db.driver.MyPostgresDriver.api._


/**
  * Created by pahn on 3/4/2559.
  */
class Departments(tag: Tag) extends Table[Department](tag, Some("public"),"departments") {

  def name = column[String]("name")
  def code = column[String]("code")


  def * = (name, code) <> (Department.tupled, Department.unapply)
}

case class Department(name:String,
                       code:String)

