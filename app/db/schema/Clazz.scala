package db.schema

import slick.lifted.Tag
import net.shantitree.flow.db.driver.MyPostgresDriver.api._

/**
  * Created by pahn on 3/4/2559.
  */
class ClazzTable (tag: Tag) extends Table[Clazz](tag, Some("public"),"clazz") {

  def id = column[String]("id", O.PrimaryKey)
  def name = column[String]("name")
  def code = column[String]("code")

  def * = (id,name, code) <> (Clazz.tupled, Clazz.unapply)
}

case class Clazz(id:String,
                  name:String,
                  code:String)


