package db.schema

import slick.lifted.Tag
import net.shantitree.flow.db.driver.MyPostgresDriver.api._

/**
  * Created by pahn on 3/4/2559.
  */
class SubjectsDepartments(tag: Tag) extends Table[SubjectDepartment](tag, Some("public"),"subjects_departments") {

  def id = column[String]("id")
  def subjectId = column[Int]("subject_id")
  def departmentId = column[String]("department_id")


  def * = (id, subjectId, departmentId) <> (SubjectDepartment.tupled, SubjectDepartment.unapply)
}

case class SubjectDepartment(id: String,
                             subjectId: Int,
                             departmentId: String)
