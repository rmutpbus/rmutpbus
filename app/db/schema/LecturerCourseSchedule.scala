package db.schema

import java.time.LocalDateTime

import slick.lifted.Tag
import net.shantitree.flow.db.driver.MyPostgresDriver.api._

class LecturerCourseScheduleTable(tag: Tag) extends Table[LecturerCourseSchedule](tag, Some("public"), "lecturer_course_schedule"){

  val lectureRoom =  column[String]("lecture_room")
  val startLecturedAt = column[LocalDateTime]("start_lectured_at")
  val subjectCode = column[String]("subject_code")
  val subjectName = column[String]("subject_name")
  val lecturerPosition = column[String]("lecturer_position")
  val lecturerFirstName = column[String]("lecturer_first_name")
  val lecturerLastName = column[String]("lecturer_last_name")
  val semester = column[Int]("semester")
  val academicYear = column[Int]("academic_year")

  def * = (lectureRoom, startLecturedAt, subjectCode, subjectName,
    lecturerPosition, lecturerFirstName, lecturerLastName, semester, academicYear) <> (LecturerCourseSchedule.tupled, LecturerCourseSchedule.unapply)

}

case class LecturerCourseSchedule(lectureRoom: String,
                                  startLecturedAt: LocalDateTime,
                                  subjectCode: String,
                                  subjectName: String,
                                  lecturerPosition: String,
                                  lecturerFirstName: String,
                                  lecturerLastName: String,
                                  semester: Int,
                                  academicYear: Int)



