package db.schema

import java.time.LocalDateTime

import slick.lifted.Tag
import net.shantitree.flow.db.driver.MyPostgresDriver.api._

/**
  * Created by pahn on 3/4/2559.
  */
class AcademicCalendarTable(tag: Tag) extends Table[AcademicCalendar](tag, Some("public"),"academic_calendar") {

  def id = column[Int]("id", O.PrimaryKey)
  def academicYear = column[Int] ("academic_year")
  def semester = column[Int]("semester")
  def event = column[String]("event")
  def startedAt= column[LocalDateTime]("started_at")
  def finishedAt = column[LocalDateTime]("finished_at")
  def durationNotes = column[String]("duration_notes")
  def eventNotes = column[String]("event_notes")

  def * = (id,academicYear, semester, event, startedAt, finishedAt, durationNotes, eventNotes) <> (AcademicCalendar.tupled, AcademicCalendar.unapply)



}

case class AcademicCalendar(id: Int,
                            academicYear:Int,
                            semester: Int,
                            event: String,
                            startedAt: LocalDateTime,
                            finishedAt: LocalDateTime,
                            durationNotes: String,
                            eventNotes:String)

