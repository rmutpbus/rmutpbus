package db.schema

import slick.lifted.Tag
import net.shantitree.flow.db.driver.MyPostgresDriver.api._


/**
  * Created by pahn on 3/4/2559.
  */
class Subjects(tag: Tag) extends Table[Subject](tag, Some("public"),"subjects") {

  def id = column[Int]("id", O.AutoInc, O.PrimaryKey)
  def name = column[String]("name")
  def code = column[String]("code")


  def * = (id, name, code) <> (Subject.tupled, Subject.unapply)
}

case class Subject(id: Int,
                   name:String,
                   code:String)

