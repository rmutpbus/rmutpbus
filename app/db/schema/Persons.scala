package db.schema

import slick.lifted.Tag
import net.shantitree.flow.db.driver.MyPostgresDriver.api._

/**
  * Created by pahn on 2/4/2559.
  */
class Persons(tag: Tag) extends Table[Person](tag, Some("public"),"persons") {
  def id = column[String]("id", O.PrimaryKey)
  def studentCode = column[String]("student_code")
  def lecturerCode = column[String]("lecturer_code")
  def titleName = column[String]("title_name")
  def firstName = column[String]("first_name")
  def lastName = column[String]("last_name")
  def facultyId = column[String]("faculty_id")
  def departmentId = column[String]("department_id")
  def studentStatus = column[String]("student_status")
  def clazzId = column[String]("clazz_id")
  def lecturerPosition = column[String]("lecturer_position")

  def * = (id, studentCode, lecturerCode, titleName, firstName, lastName, facultyId, departmentId, clazzId, lecturerPosition) <> (Person.tupled, Person.unapply)



}

case class Person(id: String,
                  studentCode: String,
                  lecturerCode: String,
                  titleName: String,
                  firstName: String,
                  lastName: String,
                  facultyId: String,
                  departmentId: String,
                  clazzId: String,
                  lecturerPosition: String)
