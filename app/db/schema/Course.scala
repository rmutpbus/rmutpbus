package db.schema

import java.time.LocalDateTime

import slick.lifted.Tag
import net.shantitree.flow.db.driver.MyPostgresDriver.api._


/**
  * Created by pahn on 3/4/2559.
  */
class Courses (tag: Tag) extends Table[Course](tag, Some("public"),"courses") {

  def subjectId = column[Int]("subject_id")
  def lectureRoom = column[String]("lecture_room")
  def lecturerId = column[String]("lecturer_id")
  def id = column[String] ("id", O.PrimaryKey)
  def departmentID = column[Char]("department_id ")
  def maximumStudents = column[Char]("maximum_students")
  def startLecturedAt = column[LocalDateTime] ("start_lectured_at")
  def semester = column[Int]("semester")
  def academicYear = column[Int]("academic_year")
  def * = (subjectId,lectureRoom,lecturerId,id,departmentID ,maximumStudents,startLecturedAt, semester, academicYear) <> (Course.tupled, Course.unapply)

}

case class Course(subjectId: Int,
                  lectureRoom: String,
                  lecturerId: String,
                  id: String,
                  departmentID: Char,
                  maximumStudents: Char,
                  startLecturedAt: LocalDateTime,
                  semester: Int,
                  academicYear: Int)








