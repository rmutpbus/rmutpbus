package db.schema

import slick.lifted.TableQuery

/**
  * Created by pahn on 2/4/2559.
  */
object Schemas {
  object pub {
    val tqPersons = TableQuery[Persons]
    val tqAcademicCalendar = TableQuery[AcademicCalendarTable]
    val tqClazz = TableQuery[ClazzTable]
    val tqCourseRegistries = TableQuery[CourseRegistries]
    val tqCourses = TableQuery[Courses]
    val tqDepartments = TableQuery[Departments]
    val tqFaculty = TableQuery[Faculties]
    val tqSubjects = TableQuery[Subjects]
    val tqSubjectDepartments = TableQuery[SubjectsDepartments]
    val vqLecturerCourseSchedule = TableQuery[LecturerCourseScheduleTable]
    val vqPersonalCourseSchedule = TableQuery[PersonalCourseScheduleTable]
  }
}
