package db.schema

import slick.lifted.Tag
import net.shantitree.flow.db.driver.MyPostgresDriver.api._



/**
  * Created by pahn on 3/4/2559.
  */
class CourseRegistries (tag: Tag) extends Table[CourseRegistry](tag, Some("public"), "course_registries") {
  def id = column[String]("id", O.PrimaryKey)
  def studentId = column[String]("student_id")
  def courseId = column[String]("course_id")


  def * = (id,studentId, courseId) <> (CourseRegistry.tupled, CourseRegistry.unapply)


}

case class CourseRegistry(id:String,
                          studentId:String,
                          courseId:String)


