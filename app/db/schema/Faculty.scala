package db.schema

import com.sun.org.apache.bcel.internal.classfile.Code
import slick.lifted.Tag
import net.shantitree.flow.db.driver.MyPostgresDriver.api._


/**
  * Created by pahn on 3/4/2559.
  */
class Faculties(tag: Tag) extends Table[Faculty](tag, Some("public"), "faculty") {

  def id = column[String]("id")
  def name = column[String]("name")
  def code = column[String]("code")


  def * = (id, name, code) <> (Faculty.tupled, Faculty.unapply)



}

case class Faculty(id: String,
                   name: String,
                   code: String)
