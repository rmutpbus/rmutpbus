package pickle
import upickle.default.Writer
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import upickle.Js

object CustomPickle {

  val dtFormatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME

  implicit val localDateTimeWriter = Writer[LocalDateTime] { dt ⇒
    Js.Str(dt.format(dtFormatter))
  }
}
