import sbt.Project.projectToRef

resolvers ++= Seq(
  "Typesafe Releases1" at "http://repo.typesafe.com/typesafe/maven-releases/",
  "Typesafe Releases2" at "http://repo.typesafe.com/typesafe/releases/",
  "OSS Sonatype Releases" at "https://oss.sonatype.org/content/repositories/releases/",
  Resolver.bintrayRepo("typesafe", "bintray-typesafe-commercial-maven-releases")
)

lazy val clientCommonSettings = Seq(
  scalaVersion := "2.11.7",
  persistLauncher := true,
  persistLauncher in Test := false,
  libraryDependencies ++= Seq(
    "biz.enef" %%% "slogging" % "0.3"
    ,"org.scala-js" %%% "scalajs-dom" % "0.8.2"
    ,"com.lihaoyi" %%% "upickle" % "0.3.8"
    ,"org.querki" %%% "jquery-facade" % "0.11"
    ,"org.scala-lang.modules" %% "scala-async" % "0.9.5"
  )
  ,addCompilerPlugin(compilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full))
)

lazy val clientMain = (project in file("client-main")).
  settings(clientCommonSettings:_*).
  settings().
  enablePlugins(ScalaJSPlugin, ScalaJSPlay).
  dependsOn(sharedJs)

lazy val clients = Seq(clientMain)

lazy val server = (project in file(".")).
  settings(
    name := "rmutpbus",
    organization := "edu.rmutpbus",
    version := "0.1",
    scalaVersion := "2.11.7",
    scalaJSProjects := clients,
    pipelineStages := Seq(scalaJSProd, gzip),
    libraryDependencies ++= Seq(
      "com.typesafe.slick" %% "slick-hikaricp" % "3.1.1"
      ,"com.typesafe.play" %% "play-slick" % "1.1.1"
      ,"com.typesafe.slick" %% "slick" % "3.1.1"
      ,"org.postgresql" % "postgresql" % "9.4.1208"
      ,"com.typesafe.slick" %% "slick" % "3.1.1"
      ,"com.github.tminglei" %% "slick-pg" % "0.12.1"
      ,"com.github.tminglei" %% "slick-pg_date2" % "0.12.1"
      ,"net.shantitree.flow" %% "db" % "0.4.6"
      ,"com.lihaoyi" %% "upickle" % "0.4.0"
      ,"com.vmunier" %% "play-scalajs-scripts" % "0.5.0"
    )
    ,addCompilerPlugin(compilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full))
  ).enablePlugins(PlayScala).
  aggregate(clients.map(projectToRef): _*).
  dependsOn(sharedJvm)

lazy val shared = (crossProject.crossType(CrossType.Pure) in file("shared")).
  settings(
    scalaVersion := "2.11.7"
  ).jsConfigure(_ enablePlugins ScalaJSPlay)

lazy val sharedJvm = shared.jvm

lazy val sharedJs = shared.js

onLoad in Global := (Command.process("project server", _: State)) compose (onLoad in Global).value

scalacOptions ++= Seq("-feature", "-deprecation", "-unchecked", "-language:dynamics")
