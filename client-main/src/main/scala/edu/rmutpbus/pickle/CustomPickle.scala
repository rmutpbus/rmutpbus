package edu.rmutpbus.pickle

import upickle.Js
import upickle.default.Reader

import scala.scalajs.js

object CustomPickle {

  implicit val jsDateReader = Reader[js.Date] {
    case Js.Str(str) ⇒ new js.Date(str)
  }

}
