package edu.rmutpbus

import edu.rmutpbus.model.LecturerCourseSchedule
import org.scalajs.dom.ext.Ajax
import scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.concurrent.Future
import upickle.default._
import pickle.CustomPickle._

/**
  * Created by bkkoo on 5/3/16.
  */
class AppRPC {
  def lecturerCourseSchedule(academicYear: Int, semester: Int, lecturerName: String): Future[Seq[LecturerCourseSchedule]]  = {
    val fResult = Ajax.get(s"/query/lecturer_course_schedule/$academicYear/$semester/$lecturerName")
    fResult.map { result ⇒
      val response = result.response.asInstanceOf[String]
      read[Seq[LecturerCourseSchedule]](response)
    }
  }

}
