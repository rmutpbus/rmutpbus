package edu.rmutpbus

import scala.scalajs.js
import org.scalajs.dom.ext.Ajax

import scalajs.concurrent.JSExecutionContext.Implicits.queue
import org.scalajs.dom.{Event, document}
import org.scalajs.dom.raw.HTMLDivElement

object Application extends js.JSApp {

  val appRPC = new AppRPC
  def main():Unit = {

    document.getElementById("read").addEventListener("click", (e: Event) ⇒ {

      val fResult = appRPC.lecturerCourseSchedule(2558, 2, "พงศกร")

      fResult.map { xs ⇒

        val div = document.getElementById("content").asInstanceOf[HTMLDivElement]
        val head = xs.head

        div.innerHTML = s"${head.lecturerFirstName}"
      }
    })
  }

}
