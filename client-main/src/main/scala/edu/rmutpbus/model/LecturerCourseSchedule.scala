package edu.rmutpbus.model

import scala.scalajs.js

case class LecturerCourseSchedule(lectureRoom: String,
                                  startLecturedAt: js.Date,
                                  subjectCode: String,
                                  subjectName: String,
                                  lecturerPosition: String,
                                  lecturerFirstName: String,
                                  lecturerLastName: String,
                                  semester: Int,
                                  academicYear: Int)

